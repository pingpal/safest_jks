package io.pingpal.safest_jks;

import org.abstractj.kalium.keys.KeyPair;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;


import javax.crypto.Cipher;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;


public class Main {



    public static void main(String[] args) {


        try {
            //Gen RSA
            java.security.KeyPair kpRSA = KeyGen.generateRSAKeyPair();
            String uid = new BigInteger(130, new SecureRandom()).toString(32);

            //make csr
            String csr = Certificate.makePEM(Certificate.generatePKCS10(kpRSA, uid));

            //send to server for signing
            PKICom pkiCom = new PKICom("http://pkiserver:8080/");
            String answer = pkiCom.reqSignCsr(csr);
            //System.out.println("Answer: " + answer);
            X509Certificate x509 = Certificate.createCertificate(answer);

            //gen curve25519
            org.abstractj.kalium.keys.KeyPair kpECC = KeyGen.generateCurve25519Key();
            String b64ECCpubKey = Util.base64Encode(kpECC.getPublicKey().toBytes());
            //System.out.println(b64ECCpubKey);

            //sign b64ECCpubKey with RSA privkey
            String b64ECCpubKeySignatute = Util.base64Encode(Certificate.sign(kpECC.getPublicKey().toBytes(), kpRSA));

            //post pubkey + signature to server
            String pubKeyAns = pkiCom.setPubEncKey(uid,b64ECCpubKey, b64ECCpubKeySignatute);
            System.out.println("regPubKeyans: " + pubKeyAns);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void testPBE(){
        org.abstractj.kalium.keys.KeyPair key = new KeyPair();
        org.abstractj.kalium.keys.PrivateKey pKey = key.getPrivateKey();
        byte[] pKeyBytes = pKey.toBytes();

        String pwd = "12";
        String original = "supersecretmessage";
        String encrypted = null;
        try {
            encrypted = PBE.getPBE().encrypt(original, pwd);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String decrypted = null;
        try {
            decrypted = PBE.getPBE().decrypt(encrypted, pwd);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("original: "  + original + " encrypted: " + encrypted + " decrypted: " + decrypted);
        try {
            int maxKeyLen = Cipher.getMaxAllowedKeyLength("AES");
            System.out.println("max: " + maxKeyLen);
        } catch (Exception e){

        }
    }


}
