package io.pingpal.safest_jks;

import java.security.*;
import java.security.spec.RSAKeyGenParameterSpec;

import org.abstractj.kalium.NaCl.Sodium;
import org.abstractj.kalium.crypto.Util;
import org.abstractj.kalium.keys.PrivateKey;
import org.abstractj.kalium.keys.PublicKey;

/**
 * Created by joakim on 26/03/15.
 *
 * Class used for creation of asymmetric encryption keys.
 */

public class KeyGen {
    final static int KEY_SIZE = 2048;

    /**
     * Generates a 2048-bit RSA keypair
     * @return the keypair
     */
    public static java.security.KeyPair generateRSAKeyPair(){

        SecureRandom random = new SecureRandom();
        RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(KEY_SIZE, RSAKeyGenParameterSpec.F4);
        KeyPairGenerator generator = null;
        try {
            generator = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            generator.initialize(spec, random);
        } catch (InvalidAlgorithmParameterException e) {
            return null;
        }

        return generator.generateKeyPair();
    }

    /**
     * Generates a kalium keypair usable with Curve25519
     * @param PublicKey publicKey - pointer to the Public Key
     * @param PrivateKey privateKey - pointer to the Private Key
     */
    public static org.abstractj.kalium.keys.KeyPair  generateCurve25519Key(){
    	org.abstractj.kalium.keys.KeyPair keyPair= new org.abstractj.kalium.keys.KeyPair();
        return keyPair;
    }
//    public static void checkKeys(String publicKey, String secretKey){
//    	Util.checkLength(publicKey.getBytes(),Sodium.PUBLICKEY_BYTES);
//    	Util.checkLength(secretKey.getBytes(),Sodium.SECRETKEY_BYTES);
//    }
//    public static void checkKey(String preSharedKey){
//    	Util.checkLength(preSharedKey.getBytes(), Sodium.SECRETKEY_BYTES);
//    }
//    public static void checkSeed(String seed){
//    	Util.checkLength(seed.getBytes(), Sodium.NONCE_BYTES);
//    }
}
