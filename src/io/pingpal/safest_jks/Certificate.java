package io.pingpal.safest_jks;



import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import javax.security.auth.x500.X500Principal;
import java.io.*;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;

/**
 * Created by joakim on 26/03/15.
 */

public class Certificate {

    /**
     * Generates a Certificate signing request
     * @param keyPair The keypair associated with the csr
     * @param cn Common Name, ie a unique id in the pki
     * @return A pem-formatted string
     * @throws Exception
     */
    public static PKCS10CertificationRequest generatePKCS10(java.security.KeyPair keyPair, String cn) throws Exception {
        //make csr
        X500Principal subject = new X500Principal ("CN=" + cn);
        ContentSigner signGen = new JcaContentSignerBuilder(Constants.ALG_SIGN).build(keyPair.getPrivate());
        PKCS10CertificationRequestBuilder builder = new JcaPKCS10CertificationRequestBuilder(subject, keyPair.getPublic());
        PKCS10CertificationRequest csr = builder.build(signGen);

       return csr;
    }

    public static String makePEM(PKCS10CertificationRequest csr) throws IOException {
        //To pem-string
        String csrString = null;
        StringWriter output = new StringWriter();
        JcaPEMWriter pem = new JcaPEMWriter(output);
        pem.writeObject(csr);
        pem.close();

        return output.getBuffer().toString();
    }

    public static byte[] sign(byte [] toSign, java.security.KeyPair pair) throws IOException{

        JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder(Constants.ALG_SIGN);
        ContentSigner signer = null;

        try {
            signer = csBuilder.build(pair.getPrivate());
        } catch (OperatorCreationException e) {

            e.printStackTrace();
        }
        OutputStream os =  signer.getOutputStream();

        os.write(toSign);

        return signer.getSignature();
    }
    /*
    public void validateChain(List<X509Certificate> chain) throws CertificateException {

        Iterator<X509Certificate> itr = chain.iterator();

        X509Certificate dad = itr.next();
        dad.checkValidity();

        while (itr.hasNext()){
            X509Certificate child = itr.next();
            child.checkValidity();
            try {
                child.verify(dad.getPublicKey());
            } catch (Exception e) {
                throw new CertificateException(e);
            }
            dad = child;
        }
    }*/

    public void validateSignature(X509Certificate cert, byte[] data, byte[] signature) throws CertificateException{

        try{
            Signature signVerifier = Signature.getInstance(Constants.ALG_SIGN);
            signVerifier.initVerify(cert);
            signVerifier.update(data);
            signVerifier.verify(signature);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public static X509Certificate createCertificate(String base64Cert) throws CertificateException{

        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        InputStream in = null;
        try {
            in = new ByteArrayInputStream(base64Cert.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);

        return cert;

    }
}
