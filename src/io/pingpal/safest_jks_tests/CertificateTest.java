package io.pingpal.safest_jks_tests;

import io.pingpal.safest_jks.Certificate;
import io.pingpal.safest_jks.KeyGen;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CertificateTest {

    @Test
    public void testGeneratePKCS10() throws Exception {

            PKCS10CertificationRequest csr = Certificate.generatePKCS10(KeyGen.generateRSAKeyPair(), "testdev");
            String s = Certificate.makePEM(csr);
            if (s == null){
                fail();
            }

    }

    public void testSign(){
        java.security.KeyPair kp = KeyGen.generateRSAKeyPair();
        byte[] signedData;
        try {
            signedData = Certificate.sign("teststring".getBytes(), kp);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}