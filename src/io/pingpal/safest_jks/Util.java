package io.pingpal.safest_jks;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by joakim on 01/04/15.
 */
public class Util {

    public static String toJson(Object pojo){
        Gson gson = new Gson();
        return gson.toJson(pojo);
    }

    public static HashMap fromJson(String jString){
        Gson gson = new Gson();
        return gson.fromJson(jString, HashMap.class);
    }

    public static byte[] base64Decode(String property) throws IOException {
        return Base64.decodeBase64(property);
    }

    public static String base64Encode(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }
}
