package io.pingpal.safest_jks;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.abstractj.kalium.NaCl.Sodium;
import org.abstractj.kalium.crypto.Box;
import org.abstractj.kalium.keys.PrivateKey;
import org.abstractj.kalium.keys.PublicKey;

public class Encryption {//wrapper av cryptoBox
//	private static final double CHUNK_OF_MEMORY_TO_SACRIFICE = 0.9;
	public static final int NONCE_LENGTH = Sodium.NONCE_BYTES;
	private static final String ENCRYPTED_POSTFIX=".enc";
	private static final String DECRYPTED_POSTFIX=".dec";
	/**
	 * Author BK 28/03/15.
	 */
	
	public static byte[] encrypt(byte[] data, PublicKey sendersPublicKey,PrivateKey myPrivateKey, byte[] nonce){
		//Should fetch keys from a session singleton maybe?
		
		Box cryptoBox =new Box(sendersPublicKey,myPrivateKey);
		byte[] buf =cryptoBox.encrypt(nonce, data);
		return buf;
	}
	public static byte[] decrypt(byte[] data, PublicKey sendersPublicKey, PrivateKey myPrivateKey, byte[] nonce){
		//Should fetch keys from a session singleton maybe?
		Box cryptoBox =new Box(sendersPublicKey,myPrivateKey);
		byte[] buf =cryptoBox.decrypt(nonce, data);
		return buf;
	}
	
	public static String encryptFile(String filename, PublicKey sendersPublicKey, PrivateKey myPrivateKey, String nonce){
//		File file = new File(filename);
//		long size =file.length();
//		if(size > Runtime.getRuntime().freeMemory()*CHUNK_OF_MEMORY_TO_SACRIFICE){
//			InsaneSizeEncryption.encrypt(filename);
//		}
		byte[] plainText = readFile(filename);
		byte[] cipherText=encrypt(plainText, sendersPublicKey, myPrivateKey, nonce.getBytes());
		writeEncryptedFile(filename+ENCRYPTED_POSTFIX, cipherText, nonce.getBytes());
		return filename+ENCRYPTED_POSTFIX;
	}
	
	public static String decryptFile(String filename, PublicKey sendersPublicKey,PrivateKey myPrivateKey){
//		File file = new File(filename);
//		long size = file.length();
//		if(size > Runtime.getRuntime().freeMemory()*CHUNK_OF_MEMORY_TO_SACRIFICE){
//			InsaneSizeEncryption.decrypt(filename);
//		}
		byte[] cipherText =null; 
		byte[] nonce=null;
		readEncryptedFile(filename, cipherText, nonce);		
		byte[] plainText=decrypt(cipherText, sendersPublicKey, myPrivateKey, nonce);
		writeFile(filename+DECRYPTED_POSTFIX, plainText);
		return filename+DECRYPTED_POSTFIX;
	}
	private static byte[] readFile(String filename){
		try {
			FileInputStream fis = new FileInputStream(filename);
			byte[] buf = new byte[fis.available()];
			fis.read(buf);	
			fis.close();
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	private static void readEncryptedFile(String filename, byte[] data, byte[] nonce){
		nonce = new byte[NONCE_LENGTH];
		byte[] buf = readFile(filename);		
		data = new byte[buf.length-NONCE_LENGTH];
		
		System.arraycopy(buf,0,nonce,0,NONCE_LENGTH);
		System.arraycopy(buf,NONCE_LENGTH,data,0,data.length);
	}
	private static void writeFile(String filename, byte[] buf){
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			fos.write(buf);	
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static void writeEncryptedFile(String filename, byte[] data, byte[] nonce){
		byte[] buf = new byte[data.length+nonce.length];
		System.arraycopy(nonce,0,buf,0,NONCE_LENGTH);//only keep correct len
		System.arraycopy(data,0,buf,NONCE_LENGTH,data.length);	
		writeFile(filename, buf);
	}

}
