package io.pingpal.safest_jks;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

/**
 * Created by joakim on 26/03/15.
 * Class used for password based encryption of strings.
 * It is limited to passwords of max 128 bit length because of limitations in java.
 * A possible solution to this is to use BouncyCastles custom API or switch to another algorithm.
 */
public class PBE {

    private static byte[] nonce;

    private static PBEParameterSpec spec;
    //private static String algorithm = "PBEWithMD5AndDES";

    private static PBE instance = null;

    private PBE(){
        Security.addProvider(new BouncyCastleProvider());
        SecureRandom random = new SecureRandom();
        nonce = new byte[8];
        random.nextBytes(nonce);
        spec = new PBEParameterSpec(nonce, 20);
    }

    /**
     * Singleton constructor
     * @return returns the PBE instance
     */
    public static PBE getPBE(){
       if(instance == null) {
           instance = new PBE();
       }
       return instance;
    }

    /**
     * Method used for encryption of strings.
     * @param property The string to be encrypted
     * @param pwd The password to base encryption on
     * @return Returns a non wrapped, Base64 encoded, AES encypted string.
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException
     */
    public String encrypt(String property, String pwd) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(Constants.ALG_PBE);
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(pwd.toCharArray()));
        Cipher pbeCipher = Cipher.getInstance(Constants.ALG_PBE);
        pbeCipher.init(Cipher.ENCRYPT_MODE, key, spec);
        return Util.base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8")));//TODO lagra nonce på cipher för entropi
    }

    /**
     * Method used for decryption of strings
     * @param property The string to be decrypted, must be in non-wrapped base64 format
     * @param pwd The password to base decryption on
     * @return Decrypted string
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public String decrypt(String property, String pwd) throws GeneralSecurityException, IOException {
        SecretKeyFactory keyFactory = null;
        try {
            keyFactory = SecretKeyFactory.getInstance(Constants.ALG_PBE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(pwd.toCharArray()));
        Cipher pbeCipher = null;
        try {
            pbeCipher = Cipher.getInstance(Constants.ALG_PBE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        pbeCipher.init(Cipher.DECRYPT_MODE, key, spec);
        return new String(pbeCipher.doFinal(Util.base64Decode(property)), "UTF-8");
    }



}
