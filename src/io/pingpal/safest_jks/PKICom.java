package io.pingpal.safest_jks;

import com.google.gson.Gson;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by joakim on 31/03/15.
 */
public class PKICom {
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private String baseUrl;
    public PKICom(String baseUrl){
       this.baseUrl = baseUrl;
    }



    private String post(String url, HashMap<String, String> hMap)  throws IOException {

        String json = Util.toJson(hMap);

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public String reqSignCsr(String csr) throws IOException {
        HashMap<String, String> hMap = new HashMap<String, String>();
        hMap.put(Constants.PKI_KEY_CSR, csr);
        String jString = post(baseUrl + Constants.PKI_ADDUSR_PATH, hMap);
        hMap = Util.fromJson(jString);
        return hMap.get(Constants.PKI_RESPONSE);
    }

    public String setPubEncKey(String uid, String pubCrypt, String pubCryptSigned) throws IOException {
        HashMap<String, String> hMap = new HashMap<String, String>();
        hMap.put(Constants.PKI_KEY_UID, uid);
        hMap.put(Constants.PKI_KEY_PUBCRYPT, pubCrypt);
        hMap.put(Constants.PKI_KEY_PUBCRYPT_SIGNED, pubCryptSigned);
        String jString = post(baseUrl + Constants.PKI_ADDENC_PATH, hMap);
        hMap = Util.fromJson(jString);
        return hMap.get(Constants.PKI_RESPONSE);
    }
    /*
    private byte[] fetchPublicKey(String tag)  throws APICallException, CertificateException{

        PublicKeyInfo keyInfo =  serverAPI.getPublicKey(tag);

        X509Certificate rootCertificate = CER.getRootCA(context);
        X509Certificate issuerCert = CER.createCertificate(keyInfo.getIssuerCertificate());
        X509Certificate clientCert = CER.createCertificate(keyInfo.getClientCertificate());

        List<X509Certificate> certificateChain = new ArrayList<X509Certificate>();

        certificateChain.add(rootCertificate);
        certificateChain.add(issuerCert);
        certificateChain.add(clientCert);

        validator.validateChain(certificateChain);

        List<X509Certificate> revoked = serverAPI.getRevokedIssuers();
        validator.validateExcluded(issuerCert,revoked);

        byte [] yourpub = keyInfo.getPublicCryptoKey();

        validator.validateSignature(clientCert,yourpub,keyInfo.getSignature());

        return yourpub;
    }
    */



}
