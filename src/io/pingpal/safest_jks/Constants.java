package io.pingpal.safest_jks;

/**
 * Created by joakim on 31/03/15.
 */
public final class Constants {
    public static final String ALG_SIGN = "SHA256withRSA";
    public static final String ALG_PBE = "PBEWITHSHA256AND256BITAES-CBC-BC";
    public static final String PKI_ADDUSR_PATH = "addusr";
    public static final String PKI_ADDENC_PATH = "addenc";
    public static final String PKI_RESPONSE = "response";
    public static final String PKI_KEY_CSR = "csr";
    public static final String PKI_KEY_UID = "uid";
    public static final String PKI_KEY_PUBCRYPT = "cryptkey";
    public static final String PKI_KEY_PUBCRYPT_SIGNED = "cryptkeysign";
}
